
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <h5 class="breadcrumbs-title">Gestión Usuarios</h5>
        <ol class="breadcrumb">
          <li><a href="#">Gestión Usuarios</a>
          </li>
          <li><a href="#">Eliminar Usuario</a>
          </li>
          <li class="active">Eliminar </li>
        </ol>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <section id="content">
    <div class="row">
      <form class="col s12">
        <div class="row">
          
          <form class="col s12">
            <div class="row">
              <div class="input-field col s6">
                <input placeholder="Buscar" id="first_name" type="text" class="validate">
              </div>
            </div>
          </form>
          
          <form id ="frm-profesor" action="?c=ControllerUsuario&a=Eliminar2" method="post" enctype="multipart/form-data">
           <div style="overflow: auto; height: 400px;">
            <table class="striped" id="tabla">
              <thead>
                <tr>
                  <th></th>
                  <th>Nombre de usuario</th>
                  <th>Correo</th>
                  <th>Perfil</th>
                </tr>
              </thead>

              <tbody>
               <?php foreach($this->model->Listar() as $r): ?>
                <?php $valor = $r->idUsuario; ?>
                <tr>
                  <td><!-- Switch -->
                    <div class="switch">
                      <label>
                        <input type="checkbox" name="idUsuario" value="<?php echo $r->idUsuario;?>">
                        <span class="lever"></span>

                      </label>
                    </div>
                    <!-- Disabled Switch --></td>
                    <td><?php echo $r->user; ?></td>
                    <td><?php echo $r->correo; ?></td>
                    <td><?php echo $r->perfil; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <button class="btn waves-effect waves-light" type="submit" name="action" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');">Eliminar
            <i class="material-icons right">delete</i>
          </button>
        </form>
      </div>
    </form>
  </div>
</section>


</div>
<script type="text/javascript">

  var busqueda = document.getElementById('first_name');
  var table = document.getElementById('tabla').tBodies[0];

  buscaTabla = function(){
    texto = busqueda.value.toLowerCase();
    var r=0;
    while(row = table.rows[r++])
    {
      if ( row.innerText.toLowerCase().indexOf(texto) !== -1 )
        row.style.display = null;
      else
        row.style.display = 'none';
    }
  }

  busqueda.addEventListener('keyup', buscaTabla);
</script>
