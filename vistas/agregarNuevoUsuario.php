 <div id="breadcrumbs-wrapper" class=" grey lighten-3">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <h5 class="breadcrumbs-title">Gestión Usuarios</h5>
        <ol class="breadcrumb">
        
        </ol>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col s12 m12 l12">
      <div id="form-login">
        <form id="frm-usuario" class="col s12" action="?c=ControllerUsuario&a=Guardar" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col s12 cyan">
              <h5 class="center login-title">Crear nuevo <hr> usuario</h5>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6">
              <label for="user " >Nombre de usuario</label>
              <input  id="user" name="user" readonly required type="text" class="validate" value="<?php echo $alm->idEmpleado; ?>">
            </div>  
            <div class="input-field col s6">
              <label for="contrasena">Contraseña</label>
              <input id="contrasena" name="contrasena" readonly required type="password" class="validate" value="<?php echo $alm->nombre; ?>">
            </div>            
          </div>
          <div class="row">
            <div class="input-field col s12">
              <label for="correo">Correo</label>
              <input id="correo" name="correo" type="email" class="validate">
            </div>
          </div>
          <div class="input-field col s12">
            <select name="perfil" class="form-control">
              <option value="" disabled selected>Tipo de Usuario</option>
              <option value="ADMINISTRADOR">ADMINISTRADOR</option>
              <option value="INVITADO">INVITADO</option>

            </select>
          </div>
         
          <div class="row">
            <div class="input-field col s6">
              <button class="waves-effect green waves-light btn" id="btnAcep" >Guardar</button>
            </div>
            <div class="input-field col s6">
              
              <a class="waves-effect waves-light btn" href="?c=ControllerUsuario&a=Index">Cancelar</a>
            </div>



            
          </div>
        </form>
      </div>
      
    </div>
  </div>
  
  
</div>





