 <div id="breadcrumbs-wrapper" class=" grey lighten-3">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <h5 class="breadcrumbs-title">Acceso Directo</h5>
        <ol class="breadcrumb">
          <li><a href="#">Acceso Directo</a>
          </li>
          <li class="active"> Ver información basica
          </li>
          
        </ol>
      </div>
    </div>
  </div>
</div>

<section>
  <div class="container">
    <div class="row">

      <br>
      <center><h5 class="blue-text"><b>Mostrar Información Básica</b></h5></center>

      <div class="divider"></div>

      <br><!--salto de linea-->
      

      <br><!--salto de linea-->

      <!--<div class="card medium">-->

        <div >
        <table id="data-table-simple"  class="striped" class="centered">

          <thead>
            <tr>
              <th scope="col" class="blue-text" ><center>IDENTIFICACION</center></th>
              <th scope="col" class="blue-text"><center><b>NOMBRE</b></center></th>
              <th scope="col" class="blue-text"><center><b>APELLIDOS</b></center></th>
              <th scope="col" class="blue-text"><center><b>PERIODO</b></center></th>
              <th scope="col" class="blue-text"><center><b>DIAS DISPONIBLE</b></center></th>
            </tr>
          </thead>
          <tbody>

            <?php foreach($this->model->ListarEmp() as $emp): ?>
              <?php $valor = $emp->idEmpleado; ?>
              <tr>
                <td ><center><?php echo $emp->idEmpleado; ?></center></td>
                <td><center><?php echo $emp->nombre; ?></center></td>
                <td><center><?php echo $emp->apellido; ?></center></td>
                <td><center><?php echo $emp->periodo; ?></center></td>
                <td><center><?php echo $emp->saldoDias; ?></center></td>
              </tr>
         
            <?php endforeach; ?>

            <!--Este script nos permite poder realizar filtros de datos-->
            <script type="text/javascript">
              var busqueda = document.getElementById('idBuscar');
             var table = document.getElementById("tabla").tBodies[0];

             buscaTabla = function(){
              texto = busqueda.value.toLowerCase();
              var r=0;
              while(row = table.rows[r++])
              {
                if ( row.innerText.toLowerCase().indexOf(texto) !== -1 )
                  row.style.display = null;
                else
                  row.style.display = 'none';
              }
            }

            busqueda.addEventListener('keyup', buscaTabla);

          </script>
        </tbody>
      </table>
      </div>

    <!--</div>-->

  </div>

</div><!--ciere del container-->
</section><!--sec-->