
<!-- //////////////////////////////////////////////////////////////////////////// -->

<style type="text/css">
  

  .ss:hover{
    background: red;
    transform:scale(1.2);
  }
</style>

<div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h2 style="color: #333333;
font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif;" class="Heading h6  center indigo-text">Bienvenidos</h2>


      <br><br>

  </div>
</div>



<!-- START CONTENT -->
<section id="content">

    <!--start container-->
    <div class="container">

        <!--chart dashboard start-->

        <!--chart dashboard end-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!--card stats start-->
        
        <!--card stats end-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!--card widgets start-->
        <div id="card-widgets" >
            <div class="row" >

                <div class="col s6 m6">
                    <ul id="task-card"  class="collection with-header" style="border-radius: 20px">
                        <li  class="collection-header  blue lighten-3" >
                            <h4 class="task-card-title"><h2 class="center light-blue-text"><img src="iconos/employees.png"></h2>
                            </h4>
                            <p class="task-card-date">Control Emplados</p>
                        </li>
                        <li class="collection-item dismissable">
                            Este módulo pretende controlar el flujo de datos para los empleados , es decir gestiona la información de cada empleado, con este apartado podra realizar acciones como agregar,modificar ,inactivar o consultar empleados.
                        </li>
                    </ul>
                </div>

                <div class="col s6 m6">
                    <ul id="task-card" class="collection with-header" style="border-radius: 20px">
                        <li class="collection-header  blue  lighten-3">
                            <h4 class="task-card-title"><h2 class="center light-blue-text"><img height="64px" width="64px" src="iconos/user.png"></h2>
                            </h4>
                            <p class="task-card-date">Control Usuarios</p>
                        </li>
                        <li class="collection-item dismissable">
                            Opción disponible unicamente para el administrador , ya que gestiona los usuarios en el sistema.
                        </li>
                    </ul>
                </div>
            </div>

            
                <div class="row">
                <div class="col s6 m6 ">
                    <ul id="task-card" class="collection with-header" style="border-radius: 20px">
                        <li class="collection-header  blue  lighten-3">
                            <h4 class="task-card-title"><h2 class="center light-blue-text"><img src="iconos/booking.png"></h2>
                            </h4>
                            <p class="task-card-date">Control de Vacaciones</p>
                        </li>
                        <li class="collection-item dismissable">
                           En este modulo se llevarán a cabo las pricipales funciones del sistema ,la cuales serian asignar vaciones a un empleado ,tomando en cuanta los registros de empleado del ministerio de salud,esto con fin de controlar de manera minusiosa las vacaciones que lleva un empleado dependiendo de su fracciones,días disponibles entre otros.
                       </li>
                   </ul>
               </div>
               <div class="col s6 m6 ">
                <ul id="task-card" class="collection with-header" style="border-radius: 20px">
                    <li class="collection-header blue  lighten-3">
                        <h4 class="task-card-title"><h2 class="center light-blue-text"><img src="iconos/globe.png"></h2>
                    </h4>
                    <p class="task-card-date">Acceso Público</p>
                </li>
                <li class="collection-item dismissable">
                   Acceso directo ,para la consulta de vacaciones , en este modulo todos los usuarios pueden acceder para realizar consultas.
               </li>
           </ul>
       </div>



   </div>
</div>
<!--card widgets end-->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!--work collections start-->

<!--work collections end-->

</div>
<!--end container-->
</section>
<!-- END CONTENT -->
