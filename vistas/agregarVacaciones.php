

 <div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Gestión Vacaciones</h5>
          <ol class="breadcrumb">
            <li><a href="#">Gestión Vacaciones</a>
            </li>
            <li><a href="#">Agregar Vacaciones</a>
            </li>
            <li class="active">Periodo</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

<div class="container">
	
	<div class="section">
		<div id="table-datatables">
          <h4 class="header">Periodos Actuales de todos los empleados</h4>
          <div class="row">
            <div class="col s6 m6">
              <p>Vista general de periodos.</p>

              
          </div>
  <!-- Modal Trigger -->
  <a  style="float:right;" class="waves-effect waves-light btn orange darken-1 modal-trigger" href="#modal1">Ver periodos por Empleado <i class="material-icons">assignment_ind</i></a>

  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
      <h4>Ingrese la cedula del empleado :</h4>
      <form action="?c=ControllerVacaciones&a=MostrarInfoEmpleado" method="post">
      <input type="text" name="ced" required onkeypress="return valida(event)" required maxlength="9">
      <button type="submit" style="float:right;" class="waves-effect waves-light btn blue darken-1 " >Buscar <i class="material-icons">assignment_ind</i></button>
      </form>
    </div>
    <div class="modal-footer">
      
    </div>
  </div>
              
          <div class="col s12 m12 ">
              <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                <thead>
                    <tr>
                        <th>Identificación</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Años Laborados</th>
                        <th>Fecha Ingreso</th>
                        <th>Descripción</th>
                        <th>Perido</th>
                        <th>Días por Periodo</th>
                        <th>Saldo Días</th>
                        <th>Acción</th>
                    </tr>
                </thead>

               

               <tbody>
                <?php foreach ($modelVac->Listar() as $f) :?>
                    <tr>
                        <td>
                            <?php echo $f->idEmpleado?>
                        </td>
                        <td>
                            <?php echo $f->nombre?>
                        </td>
                        <td>
                            <?php echo $f->apellido?>
                        </td>
                        <td>
                            <?php echo $f->anosLaborado?>
                        </td>
                        <td>
                            <?php echo $f->fechaIngreso?>
                        </td>
                        <td>
                            <?php echo $f->descripcion?>
                        </td>
                        <td>
                            <?php echo $f->periodo?>
                        </td>
                        <td>
                            <?php echo $f->diasTotalesPorPeriodo?>
                        </td>
                        <td>
                            <?php echo $f->saldoDias?>
                        </td>
                        <td>
                          <a  href="?c=ControllerVacaciones&a=MostrarInfo&idEmpleado=<?php echo $f->idEmpleado?>&idFracciones=<?php echo $f->idFracciones;?>" class="waves-effect waves-light btn  light-green"><i class="material-icons">info</i>ver</a></td>
                       <!-- <td><a onclick="crearCard()"; href="#cart" class="btn  indigo darken-1"><i class="material-icons">add</i></a></td>-->
                    </tr>
                <?php  endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div> 
<div id="cart" name="cart"></div>

</div> <!-- Modal Trigger -->
 

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Fracción</h4>
      <div class='row' >
    <div class='col s12 m12'>
      <div class='card '>
        <div class='card-content '>
          <span class='card-title'>Fracción</span>
          <div class='row'><div class='col s6'><label >Fecha de salida</label><input id='idnacimiento' type='date' class='datepicker' name='pnacimiento' required=></div><div class='col s6'><label >Fecha de entrada</label><input id='idingreso'type='date'   class='datepicker' name='ingreso' required></div></div>
          <div class='row'><label >Descripción</label><textarea></textarea></div>
        </div>
        <div class='card-action'>
          <a href='#'>Asignar</a>
        
        </div></div></div></div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

</div> 

<script type="text/javascript">

$(document).ready(function(){
    crearCard();
});
    function crearCard() {
        var ob= document.getElementById("cart");
        var nuevo=document.createElement("div");
        nuevo.innerHTML="<div class='row' >"+
    "<div class='col s12 m12'>"+
      "<div class='card '>"+
        "<div class='card-content white-text'>"+
          "<span class='card-title'>Fracción</span>"+
          "<div class='row'><div class='col s6'><label >Fecha de salida</label><input id='idnacimiento' type='date' class='datepicker' name='pnacimiento' required=></div><div class='col s6'><label >Fecha de entrada</label><input id='idingreso'type='date'   class='datepicker' name='ingreso' required></div></div>"+
          "<div class='row'><label >Descripción</label><input type='textarea' ></div>"+
        "</div>"+ 
        "<div class='card-action'>"+
          "<a href='#'>This is a link</a>"+
          "<a href='#'>This is a link</a>"+
        "</div></div></div></div>";

        $('#cart').empty();
        ob.appendChild(nuevo);
    }
   
</script>
<script>
  function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
      return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
  }
</script>