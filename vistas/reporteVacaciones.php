
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
  <div class="container">
    <div class="row">
      <div class="col s12 m12 l12">
        <h5 class="breadcrumbs-title">Gestión Vacaciones</h5>
        <ol class="breadcrumb">
          <li><a href="#">Gestión Vacaciones</a>
          </li>
          <li><a class="active">Generar Reporte</a>
          </li>

        </ol>
      </div>
    </div>
  </div>
</div>

<div class="card-panel">
<div class="container">
  <div class="row">
    
    <div class="col s12 m12 l12">
      <div id="form-login">
        <form id="formulario" class="col s12"  method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col s12 lime lighten-5 ">
              <h5 class="">Buscar Empleado</h5>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <label for="ced">Cedula</label>
              <input  id="ced" name="ced" required type="text"  onkeypress="return valida(event)" class="validate">

             <center> <button  class="waves-effect waves-light btn red" id="send" type="submit" ><li class="material-icons center">search</li>Buscar</button></center>
            </div>
           
          </div>  
          
        </form> <div id="mensajes"></div>
      </div>

    </div>
  </div>
</div>
</div>



<script  src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 



<script type="text/javascript">
  var messageConfirm;
  $(document).on("ready", function(){
    enviarMensaje();
    
  });

  var enviarMensaje=function () {
    $('#send').on("click",function(e){
      e.preventDefault();
      var frm=$('#formulario').serialize();
      $.ajax({
        type: "POST",
        url: "?c=ControllerVacaciones&a=cargarDatos",
        data: frm,
        dataType:"html",
        success: function(data)
        {
          console.log(frm);
        }
      }).done(function(info){

        var div=document.getElementById("mensajes");
        var d= document.createElement("div");
        if($("#smsConfirmacion").length) {
          $('#smsConfirmacion').remove();
        }
        d.innerHTML="<center><div id='cargador'><p>Cargando...</p><div class='preloader-wrapper big active'>"+
        "<div class='spinner-layer spinner-blue-only'>"+
        "<div class='circle-clipper left'>"+
        "<div class='circle'></div>"+
        "</div><div class='gap-patch'>"+
        "<div class='circle'></div>"+
        "</div><div class='circle-clipper right'>"+
        "<div class='circle'></div>"+
      "</div></div></div></div></center>";//crea el 
        div.appendChild(d);//agrega el preloader al html
        messageConfirm=info;
        setTimeout ("limpiar()", 1000);//muestra el preloader y luego ejecuta la funcion
        
        
        
      })
    });
  }
  function limpiar(){
    var div=document.getElementById("cargador");
    div.remove();
    $('#mensajes').html(messageConfirm);
  }

</script>

<script>
  function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
      return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
  }
</script>
