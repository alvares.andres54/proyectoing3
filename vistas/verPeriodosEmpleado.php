

 <div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Gestión Vacaciones</h5>
          <ol class="breadcrumb">
            <li><a href="#">Gestión Vacaciones</a>
            </li>
            <li><a href="#">Agregar Vacaciones</a>
            </li>
            <li ><a href="?c=ControllerVacaciones&a=Index">Periodos generales</a></li>
            <li class="active">Periodos por empleado</li>
          </ol>
        </div>
      </div>
    </div>
  </div>

<div class="container">
	<a class="btn red" href="?c=ControllerVacaciones&a=Index"><i class="material-icons">arrow_back</i>Regresar</a>
	<div class="section">
		<div id="table-datatables">
          <h4 class="header">Periodo por empleado</h4>
          <div class="row">
            <div class="col s12 m12">
              <p></p>
          </div>
  
  <div class="row">
  <div class="col s6">
              <label >Cedula:<?php echo $empModel->idEmpleado ;?></label><br>
              <label >Nombre:<?php echo $empModel->nombre ;?></label><br>
              <label >Apellidos:<?php echo $empModel->apellido ;?></label><br>
            </div>
            <div class="col s6">
               <label >Fecha Ingreso:<?php echo $empModel->fechaIngreso ;?></label><br>
              <label >Unidad de trabajo:<?php echo $empModel->unidadTrabajo ;?></label><br>
              <label >Área de trabajo:<?php echo $empModel->areaTrabajo ;?></label><br>
            </div>
  </div>

              <hr>
          <div class="col s12 m12 ">
              <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                <thead>
                    <tr>
                        
                        <th>Descripción</th>
                        <th>Perido</th>
                        <th>Días por Periodo</th>
                        <th>Saldo Días</th>
                        <th>Acción</th>
                    </tr>
                </thead>

               

               <tbody>
                <?php foreach ($modelVac->ListarPeriodoEmpleado($emp) as $f) :?>
                    <tr>
                       
                        <td>
                            <?php echo $f->descripcion?>
                        </td>
                        <td>
                            <?php echo $f->periodo?>
                        </td>
                        <td>
                            <?php echo $f->diasTotalesPorPeriodo?>
                        </td>
                        <td>
                            <?php echo $f->saldoDias?>
                        </td>
                        <td>
                          <a  href="?c=ControllerVacaciones&a=MostrarInfo&idEmpleado=<?php echo $f->idEmpleado?>&idFracciones=<?php echo $f->idFracciones;?>" class="waves-effect waves-light btn  light-green"><i class="material-icons">info</i></a></td>
                       <!-- <td><a onclick="crearCard()"; href="#cart" class="btn  indigo darken-1"><i class="material-icons">add</i></a></td>-->
                    </tr>
                <?php  endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div> 
<div id="cart" name="cart"></div>

</div> <!-- Modal Trigger -->
 

  <!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
      <h4>Fracción</h4>
      <div class='row' >
    <div class='col s12 m12'>
      <div class='card '>
        <div class='card-content '>
          <span class='card-title'>Fracción</span>
          <div class='row'><div class='col s6'><label >Fecha de salida</label><input id='idnacimiento' type='date' class='datepicker' name='pnacimiento' required=></div><div class='col s6'><label >Fecha de entrada</label><input id='idingreso'type='date'   class='datepicker' name='ingreso' required></div></div>
          <div class='row'><label >Descripción</label><textarea></textarea></div>
        </div>
        <div class='card-action'>
          <a href='#'>Asignar</a>
        
        </div></div></div></div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

</div> 

<script type="text/javascript">

$(document).ready(function(){
    crearCard();
});
    function crearCard() {
        var ob= document.getElementById("cart");
        var nuevo=document.createElement("div");
        nuevo.innerHTML="<div class='row' >"+
    "<div class='col s12 m12'>"+
      "<div class='card '>"+
        "<div class='card-content white-text'>"+
          "<span class='card-title'>Fracción</span>"+
          "<div class='row'><div class='col s6'><label >Fecha de salida</label><input id='idnacimiento' type='date' class='datepicker' name='pnacimiento' required=></div><div class='col s6'><label >Fecha de entrada</label><input id='idingreso'type='date'   class='datepicker' name='ingreso' required></div></div>"+
          "<div class='row'><label >Descripción</label><input type='textarea' ></div>"+
        "</div>"+ 
        "<div class='card-action'>"+
          "<a href='#'>This is a link</a>"+
          "<a href='#'>This is a link</a>"+
        "</div></div></div></div>";

        $('#cart').empty();
        ob.appendChild(nuevo);
    }
   
</script>
<script>
  function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
      return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
  }
</script>