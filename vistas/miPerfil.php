 
<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Mi Perfil</h5>
          <ol class="breadcrumb">

            <li class="active">Acerca de mi</li>
        </ol>
    </div>
</div>
</div>
</div>
<div >

 <div >
    <div class="row">

        <div class="col s12 m6  offset-s2">
            <div id="profile-card" class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="img/back.jpg" alt="user background">
                </div>
                <div class="card-content">
                    <img src="img/login.png" alt="" class="circle responsive-img activator card-profile-image">
                    <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                        <i class="mdi-editor-mode-edit"></i>
                    </a>

                    <span class="card-title activator grey-text text-darken-4"><?php echo $_SESSION['sesionUsuario']; ?></span>
                    <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i>Rol : <?php echo $_SESSION['perfil_']; ?></p>
                    
                    <p><i class="mdi-communication-email cyan-text text-darken-2"></i><?php echo $_SESSION['email']; ?></p>

                </div>
                <div class="card-reveal">
                    <form action="?c=ControllerLogin&a=Actualizar" method="post">
                    <span class="card-title grey-text text-darken-4"><?php echo $_SESSION['sesionUsuario']; ?> <i class="mdi-navigation-close right"></i></span>
                    <p>Etidar usuario</p> 
                    <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i></p>
                    <div class="row">
                        <div class="input-field col s12">
                          <input id="user" name="user" type="text" required value="<?php echo $_SESSION['sesionUsuario']; ?>">
                          <label for="first_name">Usuario</label>
                      </div>
                  </div>
                  <p><i class="mdi-action-lock cyan-text text-darken-2"></i>  
                    <div class="row">
                        <div class="input-field  col s12">
                          <input id="contra" required type="password" name="contra" value="<?php echo $_SESSION['contra'];?>" type="text">
                          <label for="first_name">Contraseña</label>
                      </div>
                  </div></p>
                  <p><i class="mdi-communication-email cyan-text text-darken-2"></i></p>
                  <div class="row">
                    <div class="input-field col s12">
                      <input  id="email" type="text" required name="email" value="<?php echo $_SESSION['email']; ?>">
                      <label for="first_name">Correo</label>
                  </div>
                 </div>
                 <div class="row">
                    <div class="input-field col s12">
                    <center>  
                        <button type="submit" class="waves-effect waves-light btn-large red darken-4"><i class="material-icons right">save</i>Guardar</button>
                    </center>
                  </div>
                 </div>
             </form>
          </div>
      </div>
  </div>

</div>
</div>

</div>