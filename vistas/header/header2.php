<?php
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

if(!isset($_SESSION['sesionUsuario'])){
    header("location: ?c=ControllerLogin&a=Index");
}

if( isset($_SESSION['sesionUsuario']) && $_SESSION['perfil_']  == "INVITADO"){
    header("location: ?c=ControllerAccesoPublico&a=Index");
  }


?>

<!DOCTYPE html>
<html lang="es">

<!--================================================================================
    Item Name: Materialize - Material Design Admin Template
    Version: 1.0
    Author: GeeksLabs
    Author URL: http://www.themeforest.net/user/geekslabs
    ================================================================================ -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
       
       
        <title>Ministerio de salud</title>

        <!-- Favicons-->
        <link rel="icon" href="img/ministerioLogo.png" sizes="32x32">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="img/ministerioLogo.png">
        <!-- For iPhone -->
        <meta name="msapplication-TileColor" content="#00bcd4">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <!-- For Windows Phone -->


        <!-- CORE CSS-->    
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">

        <link href="http://cdn.datatables.net/1.10.6/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

        <link rel="stylesheet" type="text/css" href="js/plugins.js">
        <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->  
        <link href="js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">  
        <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


    </head>

    <body>
        <!-- Start Page Loading -->
        <div id="loader-wrapper">
            <div id="loader"></div>        
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <!-- End Page Loading -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START HEADER -->
        <header id="header" class="page-topbar">
            <!-- start header nav-->
            <div class="navbar-fixed">
                <nav class="cyan">
                    <div class="nav-wrapper indigo darken-4">
                        <h1 class="logo-wrapper"><a href="?c=ControllerInicio&a=Index" class="brand-logo darken-1"><img style="height: 48px;width: 48px" src="images/ministerioLogo.png" alt="materialize logo">Ministerio de Salud</a> <span class="logo-text">Materialize</span></h1>
                        <ul class="right hide-on-med-and-down">

                            

                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end header nav-->
        </header>
        <!-- END HEADER -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START MAIN -->
        <div id="main" >
            <!-- START WRAPPER -->
            <div class="wrapper">

                <!-- START LEFT SIDEBAR NAV-->
                <aside id="left-sidebar-nav" >
                    <ul id="slide-out" class="side-nav fixed leftside-navigation" >
                        <li class="user-details cyan darken-2">
                            <div class="row">
                                <div class="col col s4 m4 l4">
                                    <img src="img/login.png" alt="" class="circle responsive-img valign profile-image">
                                </div>
                                <div class="col col s8 m8 l8">
                                    <ul id="profile-dropdown" class="dropdown-content">



                                        <li><a href="?c=ControllerLogin&a=MiPerfil"><i class="mdi-action-face-unlock"></i> Perfil</a>
                                        </li>
                                        
                                        <li><a href="?c=ControllerLogin&a=cerrarSesion"><i class="mdi-hardware-keyboard-tab"></i> Salir</a>
                                        </li>     


                                    </ul>
                                    <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $_SESSION['sesionUsuario'];?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                                    <p class="user-roal"><?php echo $_SESSION['perfil_'];?></p>
                                </div>
                            </div>
                        </li>
                        <li class="bold"><a href="?c=ControllerInicio&a=Index" class="waves-effect waves-cyan"><i class="mdi-action-home"></i> Inicio</a>
                        </li>
                        
                        <ul id="nav-mobile" class="sidenav sidenav-fixed">
                            <li class="no-padding">
                                <ul class="collapsible collapsible-accordion">

                                 <li class="bold"> <a type="button" class= "collapsible-header  waves-effect waves-cyan"  id="Emple" ><i class="material-icons">assignment_ind</i>Gestión Empleados</a> <div class="collapsible-body">
                                    <ul>
                                        <li><a  href="?c=ControllerEmpleado&a=Agregar">Agregar Empleado</a>
                                        </li>
                                        <li><a href="?c=ControllerEmpleado&a=Actualizar">Actualizar Empleado</a>
                                        </li>
                                        <li><a href="?c=ControllerCambiarEstado&a=visualizar">Empleados Activos</a>
                                        </li>

                                        <li><a href="?c=ControllerCambiarEstado&a=noActivo">Empleados inactivos</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a type="button" class="collapsible-header  waves-effect waves-cyan"><i class="material-icons">group</i> Gestión Usuarios</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a  href="?c=ControllerUsuario&a=Index">Agregar Usuario</a>
                                        </li>
                                        <li><a href="?c=ControllerUsuario&a=Eliminar">Eliminar Usuario </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="material-icons">airplanemode_active</i> Gestión Vacaciones</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="?c=ControllerVacaciones&a=Index">Agregar Vacaciones</a>
                                        </li>
                                        <li><a href="?c=ControllerVacaciones&a=vistaReporte">Generar Reporte </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li id="cambio" class="bold"><a  class="collapsible-header  waves-effect waves-cyan"><i class="material-icons">public</i> Acceso Directo</a>

                                <div class="collapsible-body">
                                    <ul>
                                        <li><a onclick="cambiocolor();" href="?c=ControllerAccesoPublico&a=Acceso">Ver</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </li>
                </ul>
                <!--pading-->
                <li class="li-hover"><div class="divider"></div></li>
                <li class="li-hover"><p class="ultra-small margin more-text"></p></li>

                <li class="li-hover"><div class="divider"></div></li>

            </ul>
            <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu" ></i></a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->




        <script>

/*if(<?php echo $_SESSION['perfil_'] ?> === "Administrador"){
document.getElementById('Emple').disabled=true;

}console.log("mnsaj");

</script>  <!-- Sidebar BSA-->
<script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
<div class="bsa-cpc"></div>
<script>
    (function(){
      if(typeof _bsa !== 'undefined' && _bsa) {
          _bsa.init('default', 'CKYD55QM', 'placement:materializecsscom', {
            target: '.bsa-cpc',
            align: 'horizontal',
            disable_css: 'true'
        });
      }
  })();

</script>



