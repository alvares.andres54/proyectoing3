
  <footer class="page-footer indigo darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Ministerio de salud</h5>
          <p class="grey-text text-lighten-4">Somos la institución que dirige y conduce a los actores sociales para el desarrollo de acciones que protejan y mejoren el estado de salud físico, mental y social de los habitantes, mediante el ejercicio de la rectoría del Sistema Nacional de Salud..
</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Versión </h5>
          <ul>
            <li><a class="white-text" href="#!">1.0</a></li>
            
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Ubicación</h5>
          <ul>
            <p>
Esquina SurOeste del Hospital Enrique Baltodano Briceño. Liberia,Guanacaste, Costa Rica . Tel 00888838 - 939394</p>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright  indigo lighten-1">
      <div class="container">
       <a class="indigo-text text-lighten-3" href="http://materializecss.com">© 2018 Copyright @INFO UNA III,Diseño y desarrollo
Grupo 8</a>
      </div>
    </div>
  </footer>
  </html>