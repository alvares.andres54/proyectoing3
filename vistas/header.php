<html>
    <head>
      <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/css_py/materialize.min.css"  media="screen,projection"/>
    <!--<link type="text/css" rel="stylesheet" href="css/style.css"/>-->
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Control & Registro de Vacaciones</title>
    </head>
      <body>
        <div>
<nav >
  <div class="nav-wrapper indigo darken-4" >
    <a href="#!" class="brand-logo"><img src="img/ministerioLogo.png" height="68px" width="68px"></a>
    <center><a href="" style="font-size: 1.5em;font-family: Courrier">Control & Registro de vacaciones</a></center>
  </div>
</nav>
