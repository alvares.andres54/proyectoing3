  <div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Gestión Empleados</h5>
          <ol class="breadcrumb">
            <li><a href="#">Gestión Empleados</a>
            </li>
            <li><a href="#">Actualizar Empleado</a>
            </li>
            <li ><a href="?c=ControllerEmpleado&a=Actualizar">Ver información de empleado</a></li>
            <li class="active">Editar Empleado</li>
          </ol>
        </div>
      </div>
    </div>
  </div>



    <div class="container">
  <div class="card-panel">
    <form id='frm-empleado' action='?c=ControllerEmpleado&a=update' method='post' enctype='multipart/form-data'>
    
        

          <h4 class="header2">Actualizar datos del empleado</h4>
          <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i></p>

          
          <div class="row">
            <div class="input-field col s6">
            <input id="cedula" type="text" readonly name="pcedula" value="<?php echo $alm->idEmpleado ?>"  onkeypress="return valida(event)" required>
            <label for="cedula">Cedula</label>
          </div>
            <div class="input-field col s6">
              <input id="nom" name="pnombre" type="text" value="<?php echo $alm->nombre; ?>">
              <label for="first_name">Nombre</label>

            </div>
            
          </div> 

          <div class="row">
            <div class="input-field col s6">
              <input id="nom" name="pApellido" type="text" value="<?php echo $alm->apellido; ?>">
              <label for="first_name">Apellido</label>
            </div>
             <div class="input-field col s6">
                      <input type="text" name="panosLaborado" id="idanos"  value="<?php echo $alm->anosLaborado; ?> "required onkeypress="return valida(event)" maxlength="2">
                      <label for="idanos">Años Laborados en otra institución</label>
                    </div>
            <div class="input-field col s6">
              <input id="nom" name="pdireccion" type="text" value="<?php echo $alm->direccion; ?>">
              <label for="first_name">Direccion</label>
            </div>
          </div> <div class="row">
          <div class="input-field col s6">
            <input type="text" name="telefono" id="telefono" value="<?php echo $alm->telefono; ?>" required onkeypress="return valida(event)">
            <label for="telefono">Telefono</label>
          </div>
          <div class="input-field col s6">
            <input type="text" name="pnumero" id="nump" value="<?php echo $alm->numeroPuesto; ?>"required onkeypress="return valida(event)">
            <label for='nump'>Número Puesto</label>
          </div></div>

        
        <div class="row">
          <div class="col s6">
            <label >Fecha de Nacimiento</label>
            <input id="idnacimiento" type="date" class="datepicker" name="pnacimiento" value="<?php echo $alm->fechaNacimiento; ?>" required=>

          </div>

          <div class="col s6">
            <label >Fecha de Ingreso</label>
            <input id="idingreso" type="date"   class="datepicker" name="ingreso" value="<?php echo $alm->fechaIngreso; ?>" fechaIngreso required>

          </div>
        </div>
        <div id="u">
          <select name="unidad" >
            <option value="" disabled>Unidad de Trabajo</option>
            <?php foreach($objUnidad->Listar() as $r): ?>

              <?php if($r->nombre==$alm->unidadTrabajo){
                echo "<option value=".$r->idUnidaOrg." selected>".$r->nombre."</option>";
              }else{
                echo "<option value=".$r->idUnidaOrg.">".$r->nombre."</option>";
              }
              ?>
              

            <?php endforeach; ?>
          </select>
        </div>
        <div id="ar">
          <select name="area">
            <option value="" disabled selected>Área de Trabajo</option>
            <?php foreach($objArea->Listar() as $r): ?>
             

              <?php if($r->nombre==$alm->areaTrabajo){
                echo "<option value=".$r->idAreaDeTrabajo." selected>".$r->nombre."</option>";
              }else{
                echo "<option value=".$r->idAreaDeTrabajo.">".$r->nombre."</option>";
              }
              ?>

            <?php endforeach; ?>
          </select>
        </div>



      <center><button  class='btn btn-success center'>Actualizar</button></center>  

      </form>
</div>
      </div>