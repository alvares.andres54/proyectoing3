<br><br><br>
<?php 
session_start();
?>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<div class="container">
  <div id="login-page " class="row" >
    <div class="offset-3 z-depth-5 card-panel">

      <div class="row">
        <div class="input-field col s6 center">
         <img  width="130" height="130" class="d-inline-block align-top" alt="" src="img/login.png"> 
         <p class="center login-form-text">Inicio de sesion</p>
       </div>
     </div>
     
     <form method="post" action="?c=ControllerLogin&a=validar" class="col s12">
      <div class="row">
        <div class="input-field col s6">
          <i class="material-icons prefix">account_circle</i>
          <input id="icon_prefix" name="user" type="text" class="validate" required="true" placeholder="Nombre de usuario" onkeypress="capLock(event)" >
        </div>
        <div class="input-field col s6">
          <i class="material-icons prefix">lock_outline</i>
          <input id="icon_telephone" name="pass"  type="password" class="validate" required="true" placeholder="Contraseña" onkeypress="capLock(event)">
        </div>
      </div>
      <?php
      if (isset($_SESSION['Mensaje'])) {
        if($_SESSION['Mensaje']=="true"){

       ?>
      <div class="alert alert-danger" role="alert">
        ¡Usuario o contraseña son incorrectos!
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<?php }}?>
<hr>
      <center><div id="aviso" style="visibility: hidden" >Block Mayús Activado</div></center>

      <div class="row">
        <div class="col s4">
        </div>
        <div class="input-field col s4">
          <button type="submit"  class="z-depth-5 btn btn-warning">Iniciar Sesión</button>
        </div>
        <div class="col s4"></div>
      </div>
      <div class="row">
        <div class="input-field col s6 m6 l6">

        </div>
        <div class="input-field col s6 m6 l6">
          <p class="margin right-align medium-small">
            <a href="?c=ControllerLogin&a=Rcontra">¿Ha olvidado su contraseña?</a>
          </p>
        </div>          
      </div>
    </form>
    
  </div>
</div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script type="text/javascript">

  function capLock(e){

   kc = e.keyCode?e.keyCode : e.which ;
    sk = e.shiftKey?e.shiftKey: ( (kc == 16) ? true : false ) ;
    if(((kc>=65&&kc<=90)&&!sk) || ((kc>=97&&kc<=122)&&sk)) 
    document.getElementById('aviso').style.visibility = 'visible';
    else document.getElementById('aviso').style.visibility = 'hidden';

  }
</script>
