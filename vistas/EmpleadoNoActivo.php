<div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Gestión Empleados</h5>
          <ol class="breadcrumb">
            <li><a href="#">Gestión Empleados</a>
            </li>
            <li><a href="#">Empleados Inactivos</a>
            </li>
            <li class="active">Visualizar</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
<section>
  <div class="container">
    <div class="row">

      <br></br>
      <center><h5 class="blue-text"><b>Lista de Empleado Inactivos</b></h5></center>

      <div class="divider"></div>

      <br></br><!--salto de linea-->
      <div class="row">
        <div class="input-field col s6">
          <input id="idBuscar" type="text" class="validate">
          <label for="idBuscar">Buscar empleado de manera especifica</label>
        </div>
      </div>

      <br></br>

        <br></br><!--salto de linea-->


        <div style="overflow: auto; height: 400px;">
          <table id="tabla"  class="striped" class="centered">

            <thead>
              <tr>
               <th scope="col" class="blue-text" ><center>CEDULA</center></th>
               <th scope="col" class="blue-text"><center><b>NOMBRE</b></center></th>
               <th scope="col" class="blue-text"><center><b>APELLIDO</b></center></th>
               <th scope="col" class="blue-text"><center><b>AÑOS LABORADO</b></center></th>
               <th scope="col" class="blue-text"><center><b>FECHA INGRESO</b></center></th>
               <th scope="col" class="blue-text"><center><b>ESTADO</b></center></th>
               <th scope="col" class="blue-text"><center><b>MOTIVO</b></center></th>
             </tr>
           </thead>
           <tbody>
            <?php foreach($this->model->ListarEmpleadoInactivo() as $emp): ?><!--solo muestra los empleado activo-->

              <tr>

                <td><center><?php echo $emp->idEmpleado; ?></center></td>
                <td><center><?php echo $emp->nombre; ?></center></td>
                <td><center><?php echo $emp->apellido; ?></center></td>
                <td><center><?php echo $emp->anosLaborado; ?></center></td>
                <td><center><?php echo $emp->fechaIngreso; ?></center></td>
                <td><center><?php if($emp->estado == 0){echo "Inactivo";}
                ?></center></td>
                <td><center><?php echo $emp->motivo; ?></center></td>
              </tr>

            <?php endforeach; ?>


            <!--Este script nos permite poder realizar filtros de datos-->
            <script type="text/javascript">
              var busqueda = document.getElementById('idBuscar');
              var table = document.getElementById("tabla").tBodies[0];

              buscaTabla = function(){
                texto = busqueda.value.toLowerCase();
                var r=0;
                while(row = table.rows[r++])
                {
                  if ( row.innerText.toLowerCase().indexOf(texto) !== -1 )
                    row.style.display = null;
                  else
                    row.style.display = 'none';
                }
              }

              busqueda.addEventListener('keyup', buscaTabla);


            </script>


          </tbody>
        </table>


    </div>

  </div>

</div><!--ciere del container-->
</section><!--sec-->