 
<section id="content">

  <!--breadcrumbs start-->
  <div id="breadcrumbs-wrapper" class=" grey lighten-3">
    <div class="container">
      <div class="row">
        <div class="col s12 m12 l12">
          <h5 class="breadcrumbs-title">Gestión Empleados</h5>
          <ol class="breadcrumb">
            <li><a href="#">Gestión Empleados</a>
            </li>
            <li><a href="#">Agregar</a>
            </li>
            <li class="active">Agregar nuevo Empleado</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--breadcrumbs end-->

  <!--start container-->
  <div class="container">
    <div class="section">

      <p class="caption">Agregar Empleado</p>

      <div class="divider"></div>
      <!--Basic Form-->
      <div id="basic-form" class="section">
        <div class="row">

          <!-- Form with placeholder -->
          <div class="col s12 m12 ">
            <div class="card-panel">
              <h4 class="header2">Ingrese los datos solicitados</h4>
              <div class="row">
                <form class="col s12" action="?c=ControllerEmpleado&a=Guardar" method="post">
                  <div class="row">
                    <div class="input-field col s6">
                      <input id="cedula" type="text" name="pcedula"  onkeypress="return valida(event)" required maxlength="9">
                      <label for="cedula">Cedula(sin guiones ,incluyendo los ceros)</label>
                    </div>

                    <div class="input-field col s6">
                      <input id="name2" type="text" name="pnombre" required maxlength="18">
                      <label for="name2">Nombre</label>
                    </div>
                  </div><!--div row-->
                  <div class="row">
                    <div class="input-field col s6">
                      <input id="n" type="text" name="pApellido" required maxlength="15">
                      <label for="n">Apellidos</label>
                    </div>

                    <div class="input-field col s6">
                      <input type="text" name="panosLaborado" id="idanos" required onkeypress="return valida(event)" maxlength="2">
                      <label for="idanos">Años Laborados en otra institución</label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="input-field col s4">
                      <input id="dir" type="text" name="pdireccion" required maxlength="15">
                      <label for="dir">Dirección</label>
                    </div>
                    <div class="input-field col s4">
                      <input type="text" name="telefono" id="telefono" required maxlength="10" onkeypress="return valida(event)">
                      <label for="telefono">Telefono</label>
                    </div>
                    <div class="input-field col s4">
                      <input type="text" name="pnumero" id="nump" required maxlength="5" onkeypress="return valida(event)">
                      <label for="nump">Número Puesto</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s6">
                      <label >Fecha de Nacimiento</label>
                      <input id="idnacimiento" type="date"  class="datepicker" name="pnacimiento" required>
                      
                    </div>

                    <div class="col s6">
                      <label >Fecha de Ingreso</label>
                      <input id="idingreso" type="date"    class="datepicker" name="ingreso" required>

                    </div>
                  </div>
                  <div class="row">
                  </div>
                  <div class="row">
                    <div id="menuUnidad" class="input-field col s6">
                      <div id="u">
                    
                        <select required name="unidad" >
                          <option value="" disabled selected>Unidad de Trabajo</option>
                          <?php foreach($objUnidad->Listar() as $r): ?>
                            <option value="<?php echo $r->idUnidaOrg?>"><?php echo $r->nombre?></option>

                        <?php endforeach; ?>
                        </select>
                      </div>

                      <div id="addUni" class="col s2">
                        <button id="btnAddUnidad" class="btn red accent-4" onclick="addUnidad();"><i class="material-icons">add</i>
                        </button>
                      </div>
                    </div>


                    <div id="menuArea" class="input-field col s6">
                      <div id="ar">
                        <select required name="area">
                          <option value="" disabled selected>Área de Trabajo</option>
                          <?php foreach($objArea->Listar() as $r): ?>
                            <option value="<?php echo $r->idAreaDeTrabajo?>"><?php echo $r->nombre?></option>

                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div id="addArea" class="col s2">
                        <button id="btnAddArea" class="btn red accent-4" onclick="addArea();"><i class="material-icons">add</i>
                        </button>
                      </div>
                    </div>

                  </div>

                  <?php
                  if (isset($_SESSION['Mensaje'])) {
                    if($_SESSION['Mensaje']=="true"){

                     ?>
                     <div class="alert alert-danger" role="alert">
                  <!--    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button> -->
                    </div>
                    <?php }}?>      
                    <br><br>
                    <div class="row">
                      <center >
                        <button style="width: 300px;height: 50px; border-radius: 50px " class="btn  light-blue accent-4" type="submit" name="action">
                         Registrar Empleado
                        </button>
                      </center>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Inline Form -->
      <!-- Inline form with placeholder -->
    </div>
  </section>
  <!--Form Advance-->          
  <script type="text/javascript">

   $('.datepicker').pickadate({
     closeOnSelect: true,
     format:'yyyy-mm-dd'
   });

   $(document).ready(function() {
    $('select').material_select();
  });

</script><!--script para materailize-->

<script type="text/javascript">
  var respaldoU=document.getElementById("u");
  var respaldoAdd=document.getElementById("addUni");
  

  function addUnidad(){

    var div= document.getElementById("menuUnidad");

    var html= document.createElement("div");


    html.innerHTML="<div id='unU' class='input-field col6' ><input type='text' required id='unidad2' name='unidad2'><label for='unidad2'>Agregar nuevo Unidad de trabajo</label> <button onclick='cancelUnidad();' class='btn red'><i class='material-icons'>close</i></button></div>";

    div.appendChild(html);
    //$('#addUni').remove();
    document.getElementById('addUni').style.visibility = 'hidden';
    //$('#u').remove();
    document.getElementById('u').style.visibility = 'hidden';
    $('#volver').remove();
  }

  function cancelUnidad(){
    $('#unU').remove();
    var div= document.getElementById("menuUnidad");
    document.getElementById('addUni').style.visibility = 'visible';
    document.getElementById('u').style.visibility = 'visible';
  }
</script>


<script type="text/javascript">
  
  function addArea(){

    var div= document.getElementById("menuArea");

    var html= document.createElement("div");


    html.innerHTML="<div id='adA' class='input-field col6' ><input type='text' required id='area2' name='area2'><label for='area2'>Agregar nuevo Area de trabajo</label><button onclick='cancelArea();' class='btn red'><i class='material-icons'>close</i></button></div>";
    div.appendChild(html);
    //$('#addArea').remove();
    //$('#ar').remove();
     document.getElementById('addArea').style.visibility = 'hidden';
    //$('#u').remove();
    document.getElementById('ar').style.visibility = 'hidden';
  }
   function cancelArea(){
    $('#adA').remove();
    var div= document.getElementById("menuUnidad");
    document.getElementById('addArea').style.visibility = 'visible';
    document.getElementById('ar').style.visibility = 'visible';
  }
</script>
<script>
  function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
      return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
  }
</script>
