<!DOCTYPE html>
<html>
<head>
  <title>Nueva Contraseña</title>
  <meta charset="utf-8">

  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection"/>
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <!--<link rel="stylesheet" href="css/styleNuevaContra.css"  type="text/css" media="screen"/>-->

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>

</head>
<body>
<br><br>
  <header>
 
 </header>

 <section>

  <div class="row" >

   <center> <div class="card medium offset-s3 yellow lighten-5 col s6" >
      <form class="col s12" action="?c=ControllerLogin&a=nuevaContrasena" method="post">
        <div class="row">

          <div class="input-field col s12">
            <input id="user" type="text" value=""  class="validate" name="puser"  maxlength="12">
            <label for="last_name">Nombre usuario</label>
          </div>
        </div>

        <div class="row">
          <div class="input-field col s12">
            <input id="password" type="password" class="validate" name="pcontrasena" maxlength="12">
            <label for="password">Ingrese Confirmación de la Nueva Contraseña</label>
          </div>
        </div>

       <center><button class="btn waves-effect waves-light" type="submit" name="action">Restablecer
          <i class="material-icons right">send</i>
        </button></center>
      </form>

    </div>
</center>
  </div><!--cierre del primer dive -->



</section>