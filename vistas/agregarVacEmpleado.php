

<div id="breadcrumbs-wrapper" class=" grey lighten-3">
	<div class="container">
		<div class="row">
			<div class="col s12 m12 l12">
				<h5 class="breadcrumbs-title">Gestión Vacaciones</h5>
				<ol class="breadcrumb">
					<li><a href="#">Gestión Vacaciones</a>
					</li>
					<li><a href="#">Agreagar Vacaciones</a>
					</li>
					<li ><a href="?c=ControllerVacaciones&a=Index">Periodos</a></li>
					<li class="active">Agregar Fracciones</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<a class="btn red" href="?c=ControllerVacaciones&a=Index"><i class="material-icons">arrow_back</i>Regresar</a>
	<hr>
	<div class="row">
		<div class="col s12 m12">
			<div id="form-logi">
				<div >
					<div class="row">
						<div class="col s6"><label >Cedula:<?php echo $emp->idEmpleado ;?></label><br>
							<label >Nombre:<?php echo $emp->nombre ;?></label><br>
							<label >Apellidos:<?php echo $emp->apellido ;?></label><br>
						</div>
						<div class="col s6">
							<label >IdFraccion:<?php echo $vac->idFracciones ;?></label><br>
							<label >Periodo:<?php echo $vac->periodo ;?></label><br>
							<label >Total días por periodo:<?php echo $vac->diasTotalesPorPeriodo ;?></label><br>
						</div>
					</div>
					
					
					<hr>
					<center>Agregar nueva fracción <a  class="waves-effect waves-light btn red modal-trigger" href="#modal1"><i class="material-icons">add</i> </a> </center>

					<table id="data-table-simple" class="responsive-table display" cellspacing="0"> 
						<thead>
							<tr>
								<th>Periodo</th>
								<th>Rige</th>
								<th>Hasta</th>
								<th>Días Solicitados</th>
								<th>Saldo dias</th>
								<th>tipo fracción</th>
							</tr>
						</thead>

					

						<tbody>
							<?php foreach ($this->modeldetalleFraccion->Listar($emp->idEmpleado,$vac->idFracciones) as $f) :?>
								<tr>
									<td>
										<?php echo $vac->periodo?>
									</td>
									<td>
										<?php echo $f->fechaSalida?>
									</td>
									<td>
										<?php echo $f->fechaEntrada?>
									</td>
									<td>
										<?php echo $f->cantDiasPorFraccion?>
									</td>
									<td>
										<?php echo $f->saldoDias?>
									</td>
									<td>
										<?php echo $f->tipofraccion?>
									</td>

									

								</tr>
							<?php  endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div id="modal1" class="modal modal-fixed-footer" >
	<div class="modal-content">
		<h4>Fracción</h4>
		<div class='row' >
			<div class='col s12 m12'>
				<div class='card '>
					<form id="formulario"  method="post">
						<div class='card-content '>
							<span class='card-title'>Fracción</span>
							<label>Cedula :</label>
							<input  name="ced" value="<?php echo $emp->idEmpleado ;?>" readonly="readonly"  />
							<label>Número Periodo :</label>
							<input  name="pperiodo" value="<?php echo $vac->idFracciones ;?>" readonly="readonly"  />
							<hr>
							<div id="menuArea" class="input-field col s12">

								<div id="ar">
									<select name="pPeriodo" required>
										<option value="" disabled selected>Periodo</option>
										<?php foreach($this->modelVac->ListarPeriodosEmpleado($emp->idEmpleado ) as $r): ?>
											<option value="<?php echo $r->idFraccion?>"><?php echo $r->periodo?></option>
										<?php endforeach; ?>
									</select>
								</div>

							</div	>
							<div class="row">
								<div class='col s12'><label >Tipo de Fracción</label>
									<select name="ptipofraccion">
										<option value="" disabled selected>Elija</option>
										<option value="OBLIGATORIA" >OBLIGATORIA</option>
										<option value="DECRETO" >DECRETO</option>
									</select>
								</div>
							</div>
							<div class='row'><div class='col s6'><label >Rige(Fecha)</label>
								<input id='idnacimiento' required max="2018" type='date' class='datepicker' name='psalida' required=>
							</div>
							<div class='col s6'><label >Hasta(Fecha)</label>
								<input id='idingreso' type='date' required   class='datepicker' name='pingreso' required>
							</div>
						</div>
						<div class='row'><label >Descripción</label>
							<textarea required name="pdescripcion" id="descripcion"></textarea>
						</div>
					</div>
					<div class='card-action'>
						<center><button id="send" class="btn blue white-text" type="submit" >Asignar vacaciones</button></center>

						<div id="mensajes"></div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<a href="#!" onclick="cerrar();" class="modal-action modal-close waves-effect waves-green btn-flat">Cerrar</a>
</div>
</div>
<script  src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 


<script type="text/javascript">
	var messageConfirm;
	$(document).on("ready", function(){
		enviarMensaje();
		
	});

	var enviarMensaje=function () {
		$('#send').on("click",function(e){
			e.preventDefault();
			var frm=$('#formulario').serialize();
			$.ajax({
				type: "POST",
				url: "?c=ControllerVacaciones&a=agregarVacaciones",
				data: frm,
				dataType:"html",
				success: function(data)
				{
					console.log(frm);
				}
			}).done(function(info){

				var div=document.getElementById("mensajes");
				var d= document.createElement("div");
				d.innerHTML="<center><div id='cargador'><p>Cargando...</p><div class='preloader-wrapper big active'>"+
				"<div class='spinner-layer spinner-blue-only'>"+
				"<div class='circle-clipper left'>"+
				"<div class='circle'></div>"+
				"</div><div class='gap-patch'>"+
				"<div class='circle'></div>"+
				"</div><div class='circle-clipper right'>"+
				"<div class='circle'></div>"+
      			"</div></div></div></div></center>";//crea el 
				div.appendChild(d);//agrega el preloader al html
				messageConfirm=info;
				setTimeout ("limpiar()", 1000);//muestra el preloader y luego ejecuta la funcion
				
				
				
			})
		});
	}
	function limpiar(){
		var div=document.getElementById("cargador");
		div.remove();
		$('#mensajes').html(messageConfirm);
	}

</script>

<script type="text/javascript">
	$('.datepicker').pickadate({
		min: new Date()
	});
</script>

<script>
                function cerrar(){
                window.location='?c=ControllerVacaciones&a=MostrarInfo&idEmpleado=<?php echo $emp->idEmpleado?>&idFracciones=<?php echo $vac->idFracciones?>';
            }
                 </script>