
<section id="content">
	<div id="breadcrumbs-wrapper" class=" grey lighten-3">
		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12">
					<h5 class="breadcrumbs-title">Gestión Empleados</h5>
					<ol class="breadcrumb">
						<li><a href="#">Gestión Empleados</a>
						</li>
						<li><a href="#">Actualizar Empleado</a>
						</li>
						<li class="active">Ver información de empleado</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="container">

		<div id="mail-app" class="section">
			<div class="row">
				<div class="col s11">
					<nav class="blue">
						<div class="nav-wrapper">
							<div class="left col s12 m5 l5">
								<ul>

									<li><a href="#!" class="email-type">Editar Empleado</a>
									</li>
									<li class="search-out">
										<input type="text" id="buscar" class="search-out-text">
									</li>
									<li>    
										<a href="javascript:void(0);" class="waves-effect waves-block waves-light show-search"><i class="mdi-action-search"></i></a>                              
									</li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
				<div class="col s12" >
					<div id="email-list" class="col s10 m4 l4 card-panel z-depth-1" style="height: 430px; overflow: auto;">
						<ul class="collection">
							<table id="tabla">
								
								<?php foreach($modelEmp->Listar() as $r): ?>
									<tr>
										<td>	
											<li class="collection-item avatar email-unread">
												<i class="mdi-action-account-box icon green-text"></i>

												<span class="email-title"><?php echo $r->nombre.' '.$r->apellido; ?></span>
												<p class="truncate grey-text ultra-small"><?php echo $r->idEmpleado; ?></p>
												<a href="#!" class="secondary-content">
													<input  onclick="ver('<?php echo $r->idEmpleado;?>','<?php echo $r->nombre.' '.$r->apellido;?>');" type=radio 
													id=<?php echo $r->idEmpleado;?> name=id
													/>
													<label for="<?php echo $r->idEmpleado; ?>"><a href="#" class="secondary-content"></a>
													</label>
												</a>
												<hr>
											</li>
										</td>
									</tr>
								<?php endforeach; ?>
							</table>
						</ul>
					</div>
					<script src="js/buscador.js"></script>
					<div id="email-details" class="col s12 m7 l7 card-panel">
						<p class="email-subject truncate">Información detallada <i class="mdi-edito-border-color yellow-text text-darken-3 right"></i>
						</p>
						<hr class="grey-text text-lighten-2">
						<div class="email-content-wrap">
							<div class="row">
								<div class="col s10 m10 l10">
									<ul class="collection">
										<li class="collection-item avatar">
											
											<form id="formulario" method="POST" role="form" > 
												<div id="car"></div>
												<img src="img/login.png" alt="" class="circle">
												<input type="text" name="nombre" readonly id="nombre" class="email-title" value="" >
												<input type="text" name="ced" id="ced" readonly class="truncate grey-text ultra-small" value="">
											</form>
										</li>
									</ul>
								</div>
								
							</div>
							<div class="email-content">

							</div>
						</div>
						<hr>
						

					</div>
				</div>
			</div>
		</section>
		<script  src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
		<script  src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script type="text/javascript">
			
			function ver(id,nombre){
				var div=document.getElementById("car");
				var d= document.createElement("div");
				d.innerHTML="<div id='cargador'><p>Cargando...</p><div  class='progress blue' ><div class='indeterminate'></div></div></div>";//crea el preloader
				div.appendChild(d);//agrega el preloader al html
				$('#mod').attr("visibility","visible");
				setTimeout ("limpiar('"+id+"','"+nombre+"')", 1000);//muestra el preloader y luego ejecuta la funcion
			}

			function limpiar(id,nombre){
				var div=document.getElementById("cargador");
				div.remove();
				$('#nombre').attr("value",nombre);
				$('#ced').attr("value",id);

				var frm=$('#formulario').serialize();
				$.ajax({
					type:"POST",
					url:"?c=controllerEmpleado&a=cargarEmpleado",
					data: frm,
					dataType:"html",
					success: function(data)
					{
						console.log(frm);
					}
				}).done(function(info){
					$('.email-content').html(info);
					
				});
			}


		</script>