
<?php
include_once "Modelo/Empleado.php";
include_once "Modelo/Fraccion.php";
include_once "Modelo/FuncionesHelp.php";
include_once "Modelo/DetalleFraccion.php";
class ControllerVacacionesController
{


	private $modelEmp;
	private $modelVac;
  private $funcionesAyuda;
  private $modeldetalleFraccion;

  public function __CONSTRUCT(){
    $this->modelEmp = new Empleado();
    $this->modelVac = new Fraccion();
    $this->funcionesAyuda = new FuncionesHelp();
    $this->modeldetalleFraccion = new DetalleFraccion();
  }

  public function Index(){

   $modelVac= new Fraccion();
   session_start();

   if(  $_SESSION['perfil_']  == "INVITADO"){
    require_once "vistas/header/headerInvitado.php";
  }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
    require_once "vistas/header/header2.php";
  }else{
    echo "ERROR_AUTENTIFICATION";
  }

  require_once "vistas/agregarVacaciones.php";
  require_once "vistas/footer/footer.php";
}

public function MostrarInfo(){

  $vac = new Fraccion();
  $emp= new Empleado();

  $vac = $this->modelVac->ObtenerPeriodo($_REQUEST['idFracciones']);
  $emp= $this->modelEmp->Obtener($_REQUEST['idEmpleado']);

  session_start();

  if(  $_SESSION['perfil_']  == "INVITADO"){
    require_once "vistas/header/headerInvitado.php";
  }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
    require_once "vistas/header/header2.php";
  }else{
    echo "ERROR_AUTENTIFICATION";
  }

  require_once "vistas/agregarVacEmpleado.php";
  require_once "vistas/footer/footer.php";


}

public function MostrarInfoEmpleado(){

 $modelVac= new Fraccion();
 $empModel= new Empleado();
 $empModel= $this->modelEmp->Obtener($_REQUEST['ced']);
 if (empty($empModel)) {
   echo "<script>
                alert('Empleado no existe');
                window.location=' ?c=ControllerVacaciones&a=Index'
                 </script>";
 }else{
   session_start();

   if(  $_SESSION['perfil_']  == "INVITADO"){
    require_once "vistas/header/headerInvitado.php";
  }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
    require_once "vistas/header/header2.php";
  }else{
    echo "ERROR_AUTENTIFICATION";
  }
  $emp=$_POST['ced'];
  require_once "vistas/verPeriodosEmpleado.php";
  require_once "vistas/footer/footer.php";
}

}
public function agregarVacaciones(){
  $ced= $_POST['ced'];
  if(empty($_POST['psalida']) || empty($_POST['pingreso']) || empty($_POST['ptipofraccion']) || empty($_POST['pdescripcion']) ){
   $this->funcionesAyuda->imprimirMensaje("Error Faltan Campos.","error.png");
 }else{
  $fechaSalida=$_POST['psalida'];
  $fechaEntrada=$_POST['pingreso'];
        //$periodo=$_POST['pPeriodo'];


  $emp= new Empleado();

       // $vac = $this->modelVac->ObtenerPeriodo($_REQUEST['idFracciones']);
  $emp= $this->modelEmp->Obtener($ced);

        if ($fechaSalida<$emp->fechaIngreso ) {//

         $this->funcionesAyuda->imprimirMensaje("Error:Fecha inicio no debe ser menor a fecha de ingreso del empleado .","error.png");
       }
       else if($fechaEntrada<$fechaSalida){
         $this->funcionesAyuda->imprimirMensaje("Error:Fechas entrada no debe ser menor a fecha de inicio .","error.png");
       }
       else{

        $vac = new Fraccion();
        $periodo=$_POST['pperiodo'];
        $tipoFraccion= $_POST['ptipofraccion'];
        $descripcion= $_POST['pdescripcion'];
        $detailFraction = new DetalleFraccion();
        $dFraccion=new DetalleFraccion();

        $saldoAnterior= $this->modeldetalleFraccion->obtenerSaldo($ced,$periodo);


        $fecha_Actual=date('Y')."-".date('m')."-".(date('d')-1);
        $nombredia= $this->funcionesAyuda->obtenerNombreDia($fechaSalida);
        $contDias=0;

        $fechaIncio=$fechaSalida;
        while ($fechaIncio<=$fechaEntrada) {
          $contDias++;
          if ($this->funcionesAyuda->obtenerNombreDia($fechaIncio)=='Domingo' || $this->funcionesAyuda->obtenerNombreDia($fechaIncio)=='Sabado') { 
                //si los dias son sabados y domingos no son contados 
            $contDias--;
          }
        //echo $fechaIncio.":".$this->funcionesAyuda->obtenerNombreDia($fechaIncio);
          $fechaIncio=$this->funcionesAyuda->sumarfecha($fechaIncio,1);
        }

        if ($contDias>$saldoAnterior->saldoDias) {// si los dias solicitados supera el saldo de dias disponibles
          $this->funcionesAyuda->imprimirMensaje("Error:Saldo de dias insuficientes para la peticion actual.","error.png");
        }else{
          $saldoActual=(int)($saldoAnterior->saldoDias-$contDias);


          $detailFraction->fracciones=$periodo;
          $detailFraction->idEmpleado=$ced;
          $detailFraction->saldoDias=$saldoActual;
          $detailFraction->cantDiasPorFraccion=$contDias;
          $detailFraction->fechaSalida=$fechaSalida;
          $detailFraction->fechaEntrada=$fechaEntrada;
          $detailFraction->tipofraccion=$tipoFraccion;
          $detailFraction->descripcion=$descripcion;
          $this->modeldetalleFraccion->agregarFraccion($detailFraction);

          $this->funcionesAyuda->imprimirMensaje("Agregada Correctamente","success.png");
        }

      }



    }
  }

  public function generarReporte(){

    require_once "Modelo/Reporte.php";
    $fecha_Actual=date('Y')."-".date('m')."-".(date('d'));
    $pdf= new  Reporte();
    $pdf->AliasNbPages();
    $pdf->AddPage();

    $empleado =$this->modelEmp->Obtener($_POST['cedu']);

    $pdf->SetFont('Arial','I',10);
    $pdf->Cell(120,10,'Cedula : '.$empleado->idEmpleado,0,1,'L');
    $pdf->Cell(120,10,'Nombre: ' .$empleado->nombre. ' '.$empleado->apellido,0,1,'L');
    $pdf->Cell(120,10,'Fecha Ingreso: ' .$empleado->fechaIngreso,0,1,'L');
    $pdf->Cell(120,10,'Realizado: '.$fecha_Actual,0,1,'R');
    $pdf->Ln(10);

    $pdf->SetFillColor(232,232,232);
    $pdf->SetFont('Arial','B',15);
    $pdf->Cell(40,6,'Periodo',1,0,'C',1);
    $pdf->Cell(40,6,'Rige',1,0,'C',1);
    $pdf->Cell(40,6,'Hasta',1,0,'C',1);
    $pdf->Cell(40,6,'Dias solicitados',1,0,'C',1);
    $pdf->Cell(30,6,'Saldo Dias',1,1,'C',1);
    $pdf->SetFont('Arial','',10);

    $ban=true;

    foreach ($this->modeldetalleFraccion->Listar($empleado->idEmpleado,0) as $f) :
      $pdf->Cell(40,6,'2017-2018',1,0,'C');
      $pdf->Cell(40,6,$f->fechaSalida,1,0,'C');
      $pdf->Cell(40,6,$f->fechaEntrada,1,0,'C');
      $pdf->Cell(40,6,$f->cantDiasPorFraccion,1,0,'C');
      $pdf->Cell(30,6,$f->saldoDias,1,1,'C');
      $ban=false;

    endforeach;
    if($ban!=false){
      $pdf->SetY(150);
      $pdf->SetX(75);
      $pdf->Cell(50,6,'No hay datos que mostrar',0,1,'C',0);
    }

    $pdf->Output();
  }

  public function vistaReporte(){
    session_start();

    if(  $_SESSION['perfil_']  == "INVITADO"){
      require_once "vistas/header/headerInvitado.php";
    }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
      require_once "vistas/header/header2.php";
    }else{
      echo "ERROR_AUTENTIFICATION";
    }
    require_once "vistas/reporteVacaciones.php";
    require_once "vistas/footer/footer.php";

  }
  public function cargarDatos(){
    $cedula=$_POST['ced'];
    if (empty($cedula)) {
     $this->funcionesAyuda->imprimirMensaje("Error:Faltan Datos .","error.png");
   }else{
    $emp= new Empleado();
    $emp= $this->modelEmp->Obtener($cedula);

    
    if (!empty($emp)) {
      $this->funcionesAyuda->imprimirMensaje("Data find success.","success.png");
      $this->funcionesAyuda->imprimirFormularioReporte($cedula,$emp->nombre,$emp->apellido);
    }else{
      $this->funcionesAyuda->imprimirMensaje("Error:Empleado no encontrado.","error.png");
    }
  }

}

}
