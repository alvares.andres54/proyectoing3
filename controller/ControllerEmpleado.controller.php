
<?php

include_once "Modelo/Empleado.php";
include_once "Modelo/UnidadTrabajo.php";
include_once "Modelo/AreaTrabajo.php";
include_once "Modelo/FuncionesHelp.php";
include_once "Modelo/Fraccion.php";

class ControllerEmpleadoController {

    public $modeloEmpleado;
    public $modeloUnidad;
    public $modeloArea;
    private $modelVac;
    private $funcionesAyuda;

    public function __contruct() {
        $this->modeloEmpleado = new Empleado();
        $this->modeloUnidad = new UnidadTrabajo();
        $this->modeloArea = new AreaTrabajo();
        $this->funcionesAyuda = new FuncionesHelp();
        $this->modelVac = new Fraccion();
    }

    public function Index() {
        require_once "vistas/header.php";
        //require_once "vistas/login.php";
        require_once "vistas/footer.php";
    }

    public function visualizarEstados() {
        $modeloEmpleado = new Empleado();
        require_once "vistas/header/header2.php";
        require_once "vistas/cambiarEstado.php";
        require_once "vistas/footer/footer.php";
    }

    public function Actualizar() {
        $modelEmp = new Empleado();
        require_once "vistas/header/header2.php";
        require_once "vistas/editarEmpleado.php";
        require_once "vistas/footer/footer.php";
    }

    public function Agregar() {
        $objUnidad = new UnidadTrabajo();
        $objArea = new AreaTrabajo();
        require_once "vistas/header/header2.php";
        require_once "vistas/agregarEmpleado.php";
        require_once "vistas/footer/footer.php";
    }

  public function Guardar(){

		$alm = new Empleado();
		$modelo = new Empleado();

		$objUnidad=new UnidadTrabajo();
		$modelUni= new UnidadTrabajo();

		$objArea= new AreaTrabajo();
		$modelArea= new AreaTrabajo();

        $data=new Empleado();
		$nacimiento =  (string)$_REQUEST['pnacimiento'];
		$trabajo  =  $_REQUEST['ingreso'];
		if($nacimiento > $trabajo){


		echo "<script>
                alert('Fecha de nacimiento mayor a fecha de ingreso');
                window.location=' ?c=ControllerEmpleado&a=Agregar'
                 </script>";

		}else{
		$alm->idEmpleado=$_REQUEST['pcedula'];
		$alm->nombre = $_REQUEST['pnombre'];
		$alm->apellido = $_REQUEST['pApellido'];
		$alm->numeroPuesto = $_REQUEST['pnumero'];
		$alm->anosLaborado = $_REQUEST['panosLaborado'];
		$alm->fechaNacimiento = (string)$_REQUEST['pnacimiento'];
		$alm->direccion = (string)$_REQUEST['pdireccion'];
		$alm->fechaIngreso = $_REQUEST['ingreso'];
		$alm->telefono=$_REQUEST['telefono'];
		$alm->encargado = 0;


		$alm->anosLaborado=(date('Y'))-(date('Y',strtotime($_REQUEST['ingreso'])));	
		
		if(!empty($_REQUEST['unidad']) && !empty($_REQUEST['area']) ){ 
			$alm->unidadOrg = $_REQUEST['unidad'];	
			$alm->areaTrabajo = $_REQUEST['area'];
			echo "Error de seleccion<br>";
		}  else{
			if(!empty($_REQUEST['unidad2']) && !empty($_REQUEST['area2']) ){ 
			$objUnidad->nombre= $_REQUEST['unidad2'];
			$objArea->nombre= $_REQUEST['area2'];

			$modelArea->Registrar($objArea);	
			$modelUni->Registrar($objUnidad);
			$objUnidad= $modelUni->ObteneIdUnidad($_REQUEST['unidad2']);
			$objArea = $modelArea->ObtenerNombre($_REQUEST['area2']);

			$alm->unidadOrg = $objUnidad->idUnidaOrg;
			$alm->areaTrabajo = $objArea->idAreaDeTrabajo;

				}else{
					$alm->unidadOrg = $_REQUEST['unidad'];	
					$alm->areaTrabajo = $_REQUEST['area'];
				}
				echo "Error con unidad2-area2<br>";
		}
		$Fraccion_anual=new Fraccion();
		$vac=new Fraccion();
		$vac->idEmpleado=$alm->idEmpleado;
		$vac->descripcion="Periodo Generado";
		$vac->periodo="2018-2019";
		if ($alm->anosLaborado>=1 && $alm->anosLaborado<6) {
			$vac->diasTotalesPorPeriodo=15;
			$vac->saldo=15;
		}
		if ($alm->anosLaborado>=6 && $alm->anosLaborado<11) {
			$vac->diasTotalesPorPeriodo=20;
			$vac->saldo=20;
		}
		if ($alm->anosLaborado>=11) {
			$vac->diasTotalesPorPeriodo=26;
			$vac->saldo=26;
		}
        if ($alm->anosLaborado<1) {
            $vac->diasTotalesPorPeriodo=0;
            $vac->saldo=0;
        }
		$data->anosLaborado=$_REQUEST['panosLaborado'];
	    $data->fechaIngreso = $_REQUEST['ingreso'];
		
		$data->idEmpleado=$_REQUEST['pcedula'];
		$modelo->Registrar($alm);
		//$modelo->ActualizarAnosLaborados($data);
		
		//echo 'unidad:'.$objUnidad->idUnidaOrg.'<br>Area:'.$objArea->idAreaDeTrabajo;
		$Fraccion_anual->agregarFraccion($vac);
		
		echo "<script>
                alert('Empleado agregado correctamente');
                window.location=' ?c=ControllerEmpleado&a=Agregar'
                 </script>";

		//header('Location: ?c=ControllerEmpleado&a=Agregar');
		//echo "Fecha nacimiento:".$alm->fechaNacimiento."<br>";
		//echo "Fecha ingreso:".$alm->fechaIngreso;
	}

	}

    public function update() {
        $alm = new Empleado();
        $modelo = new Empleado();
        $alm->idEmpleado = $_REQUEST['pcedula'];
        $alm->nombre = $_REQUEST['pnombre'];
        $alm->apellido = $_REQUEST['pApellido'];
        $alm->numeroPuesto = $_REQUEST['pnumero'];
        $alm->fechaNacimiento = (string) $_REQUEST['pnacimiento'];
        $alm->direccion = (string) $_REQUEST['pdireccion'];
        $alm->fechaIngreso = (string) $_REQUEST['ingreso'];
        $alm->unidadOrg = $_REQUEST['unidad'];
        $alm->areaTrabajo = $_REQUEST['area'];
        $alm->telefono = $_REQUEST['telefono'];
        $modelo->ActualizarEmpleado($alm);
        header('Location: ?c=ControllerInicio&a=Index');
    }

    public function cargarEmpleado() {
        $cedula = $_POST['ced'];
        $nombre = $_POST['nombre'];
        $objUnidad = new UnidadTrabajo();
        $alm = new Empleado();
        $temp = 0;
        $modelo = new Empleado();

        if ($cedula != null) {
            $f = $modelo->Obtener($cedula);

            if ($f != null) {
                echo "
				<form id='frm-empleado' action='?c=ControllerEmpleado&a=modificarEmp' method='post' enctype='multipart/form-data'>
				<div class='input-field col s6'>
				<input id='cedula' type='text' readonly name='pcedula' value=$f->idEmpleado onkeypress='return valida(event)' required>

				</div>
				<table class='responsive-table'>

				<thead >
				<th>Número Empleado</th>
				<th>Area de trabajo</th>
				<th>Años laborados</th>
				<th>Fecha nacimiento</th>
				<th>Fecha Ingreso</th>
				<th></th>
				</thead>
				<tbody>
				<tr>
				<td>" . $f->numeroPuesto . "</td>
				<td>" . $f->areaTrabajo . "</td>
				<td>" . $f->anosLaborado . "</td>
				<td>" . $f->fechaNacimiento . "</td>
				<td>" . $f->fechaIngreso . "</td>
				<td><button class='btn blue'><i class='material-icons'>editar</i></button></td>
				</tr>
				</tbody>

				</form> "
                ;
            }
        } else {
            echo "<center><h3>Seleccione el empleado a modificar.</h3></center>";
        }
        if ($temp == 1) {
            echo"<button class='btn blue' $temp=1><i class='material-icons'>editar</i></button>";
        }
    }

    public function modificarEmp() {
        $alm = new Empleado();
        $modelo = new Empleado();

        $objUnidad = new UnidadTrabajo();

        $objArea = new AreaTrabajo();
        $cedula = $_REQUEST['pcedula'];
        if ($cedula != null) {
            $alm = $modelo->Obtener($cedula);
        }require_once "vistas/header/header2.php";
        require_once "vistas/modificarEmpleado.php";
        require_once "vistas/footer/footer.php";
    }

    public function EditarEstado() {
        //$alm = new Empleado();
        //$model = new Empleado;

        $pcedula = $_REQUEST['cedulaEmpleado'];

        /* if($pcedula == null){

          echo "<center><h3>sksks.</h3></center>";

          }else{ */
        //$modelo_= new Empleado();
        ///$modelo_->idEmpleado=$_REQUEST['cedulaEmpleado'];
        $modeloEmp = new Empleado();
        $modeloEmp->Actualizar($pcedula);

        header('Location: ?c=ControllerEmpleado&a=visualizarEstados');
    }

}
?>