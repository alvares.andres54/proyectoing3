<?php

include_once 'Modelo/usuario.php';

class ControllerLoginController
{
	
	private $UsuarioModelo;
	
	public function __contruct(){
		$this->UsuarioModelo = new Usuario();
	}

	public function Index()
	{
		require_once "vistas/header.php";
		require_once "vistas/login.php";
		require_once "vistas/footer.php";
	}
	public function MiPerfil(){
		session_start();

		if(  $_SESSION['perfil_']  == "INVITADO"){
			require_once "vistas/header/headerInvitado.php";
		}else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
			require_once "vistas/header/header2.php";
		}else{
			echo "ERROR_AUTENTIFICATION";
		}
		require_once "vistas/miPerfil.php";
		require_once "vistas/footer/footer.php";

	}
	public function validar()
	{
		session_start();
		$user =$_POST['user'];
		$password=$_POST['pass'];
		if (!empty($user) || !empty($password)) {

			$usuario=new Usuario();
			$usu= $usuario->Verificar($user,$password);
			echo "user:".$usu->user;
			echo "pas:".$usu->contrasena;


			if(!empty($usu)){
				$_SESSION['idUser']=$usu->idUsuario;
				$_SESSION['sesionUsuario']=$usu->user;
				$_SESSION['email']=$usu->correo;
				$_SESSION['contra']=$usu->contrasena;
				$_SESSION['perfil_']=$usu->perfil;
				header("location: ?c=ControllerInicio&a=Index");
			}else{
				$_SESSION['Mensaje']="true";
				header("location: ?c=ControllerLogin&a=Index");
			}

		}
	}
	public function Actualizar(){
		try{
			session_start();
			$user =$_POST['user'];
			$password=$_POST['contra'];
			$email=$_POST['email'];

			if (!empty($user) || !empty($password)) {

				$usuario=new Usuario();
				$usu = new Usuario();

				$usu->idUsuario=$_SESSION['idUser'];
				$usu->user = $user;
				$usu->contrasena=$password;
				$usu->correo=$email;
				$usu->perfil=$_SESSION['perfil_'];

				
				if($usuario->Actualizar($usu)){

					header("location: ?c=ControllerLogin&a=MiPerfil");
					$_SESSION['sesionUsuario']=$usu->user;
					$_SESSION['email']=$usu->correo;
					$_SESSION['contra']=$usu->contrasena;
					
				}else{


				}

			}	
		}catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Rcontra()
	{
		require_once "vistas/header.php";
		require_once "vistas/prueba.php";
		require_once "vistas/footer.php";

	}
	public function newContra(){

		require_once "vistas/header.php";
		require_once "vistas/nuevaContra.php";
		require_once "vistas/footer.php";
	}
	public function cerrarSesion(){
		session_destroy();
		header("location: ?c=ControllerLogin&a=Index");
	}


	public function validarEmpleado(){

		$alm = new Usuario();

		$pcedula = $_REQUEST['pcedula'];

		$emp = $alm->VerificarUsuario($pcedula);

		if(!empty($emp)){

			header("location: ?c=ControllerLogin&a=newContra");

		}else{


			echo "<script>
			alert('¡Usuario no existe!');
			window.location=' ?c=ControllerLogin&a=Index'
			</script>";


		}

	}
	public function nuevaContrasena(){


		$amd = new usuario();
		$aml = new Usuario();
		$user = $_REQUEST['puser'];
		$passw =  $_REQUEST['pcontrasena'];


		if($user && $passw != null){

			$emp = $aml->VerificarUsuario($user);

			if(!empty($emp)){
				$amd->user = $user;
				$amd->contrasena = $passw;
				$aml->ActualizaContra($amd);

				echo "<script>
				alert('¡Cambio Realizado!');
				window.location=' ?c=ControllerLogin&a=Index'
				</script>";

			}else{


				echo "<script>
				alert('¡Usuario Incorrecto!');
				window.location=' ?c=ControllerLogin&a=Rcontra'
				</script>";
			}

		}else{

			echo "<script>
			alert('Por favor, debe escribir en el campo solicitado.');
			window.location=' ?c=ControllerLogin&a=newContra'
			</script>";

		}

	}/*fin del metodo*/
}

?>