<?php
	include_once "Modelo/Publico.php";

class ControllerAccesoPublicoController
{
	private $model;

	public function __CONSTRUCT(){

		$this->model = new Publico();
	}
	
	

	public function Index(){
		session_start();

   if(  $_SESSION['perfil_']  == "INVITADO"){
    require_once "vistas/header/headerInvitado.php";
  }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
    require_once "vistas/header/header2.php";
  }else{
    echo "ERROR_AUTENTIFICATION";
  }
		require_once "vistas/ErrorAcceso.php";
		require_once "vistas/footer/footer.php";
	}
	

	public function Acceso()
	{
		session_start();

   if(  $_SESSION['perfil_']  == "INVITADO"){
    require_once "vistas/header/headerInvitado.php";
  }else if(  $_SESSION['perfil_']  == "ADMINISTRADOR"){ 
    require_once "vistas/header/header2.php";
  }else{
    echo "ERROR_AUTENTIFICATION";
  }
		require_once "vistas/accesoPublico.php";
		require_once "vistas/footer/footer.php";

	}


}

?>