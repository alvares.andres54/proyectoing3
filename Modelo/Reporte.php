<?php


	require_once "FPDF/fpdf.php";
	
	class Reporte extends FPDF
	{
		
		function Header()
		{
			$this->Image('img/ministerioLogo.png',6,0,20);	
			$this->SetFont('Arial','B',15);
			$this->Cell(30);
			$this->Cell(120,10,'Ministerio de Salud',0,1,'C');
			$this->Cell(30);
			$this->Cell(120,10,utf8_decode('Dirección Regional de Rectoria de Salud la Región Chorotega'),0,1,'C');
			$this->Cell(30);
			$this->Cell(120,10,utf8_decode('Proceso Gestión de Recursos Humanos'),0,1,'C');
			$this->Cell(30);
			$this->Cell(120,10,'Estudio de Vacaciones',0,0,'C');

			$this->Ln(20);
		

			
		}


		function  Footer(){
			$this->SetY(-40);
			$this->SetFont('Arial','I',10);

			$this->Cell(120,10,'______________________',0,1,'L');
			$this->Cell(120,10,'Elaborado por',0,1,'L');
			$this->Cell(0,10,'Pag '.$this->PageNo().'/{nb}',0,0,'C');
		}
	}

	

?>