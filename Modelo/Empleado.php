<?php


class Empleado
{
	private $pdo;
	
	public $idEmpleado;
	public $nombre;
	public $apellido;
	public $numeroPuesto;
	public $anosLaborado;
	public $unidadOrg;
	public $areaTrabajo;
	public $usuario;
	public $estado;
	public $fechaNacimiento;
	public $direcion;
	public $fechaIngreso;
	public $telefono;
	public $encargado;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}




	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM empleado where estado = 1");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			->prepare("SELECT idEmpleado,empleado.nombre,apellido,numeroPuesto,anosLaborado,unidaorg.nombre as unidadTrabajo,areadetrabajo.nombre as areaTrabajo,estado,fechaNacimiento,direccion,fechaIngreso,telefono 
				from empleado 
				inner join unidaOrg on empleado.unidadOrg=unidaorg.idUnidaOrg 
				inner join areadetrabajo on empleado.areaTrabajo=areadetrabajo.idAreaDeTrabajo
				where idEmpleado= ? ");


			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(Empleado $data)
	{
		try 
		{
			$sql = "INSERT INTO empleado (idEmpleado,nombre,apellido,numeroPuesto,anosLaborado,unidadOrg,areaTrabajo,estado,fechaNacimiento,direccion,fechaIngreso,telefono,encargado) 
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->idEmpleado, 
					$data->nombre, 
					$data->apellido,
					$data->numeroPuesto,
					$data->anosLaborado,
					$data->unidadOrg,
					$data->areaTrabajo,
					
					1,
					$data->fechaNacimiento,
					$data->direccion,
					$data->fechaIngreso,
					$data->telefono,
					$data->encargado
				)
			);
			return "HECHO";
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}/*metodo para agregar empleado*/
	public function Actualizar($data,$dat)
	{

		try 
		{

			$sql = "UPDATE empleado SET 
			
			estado = ? ,
			motivo=?

			WHERE idEmpleado= ?";

			$this->pdo->prepare($sql)
			->execute(
				array(
					0,
					$dat->motivo,
					$data
				)
			);
			return true;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}/*mrtodo modificar*/

public function ActualizarAnosLaborados($anos)
	{

		try 
		{

			$sql = "UPDATE empleado set anosLaborado=?+ TIMESTAMPDIFF(YEAR,?,CURDATE()) where idEmpleado=?;  ";

			$this->pdo->prepare($sql)
			->execute(
				array(
					
					$anos->anosLaborado,
					$anos->fechaIngreso,
					$anos->idEmpleado
				)
			);
			return true;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}/*mrtodo modificar*/

	public function ListarEmpleadoActivo()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM empleado where estado = 1");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarEmpleadoInactivo()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM empleado where estado = 0");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
public function ActualizarEmpleado($data)
	{
		try 
		{
			$sql = "UPDATE empleado SET 
						nombre          = ?, 
						apellido        = ?,
                        numeroPuesto    = ?,
                        anosLaborado    = ?,
                        fechaNacimiento = ?,
                        direccion       = ?,
                        fechaIngreso    = ?,
                        telefono		= ?,
                        unidadOrg       = ?,
                        areaTrabajo     = ?

				    WHERE idEmpleado = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                  
					$data->nombre, 
					$data->apellido,
					$data->numeroPuesto,
					$data->anosLaborado,
					
					$data->fechaNacimiento,
					$data->direccion,
					$data->fechaIngreso,
					$data->telefono,
					$data->unidadOrg,
					$data->areaTrabajo,
					$data->idEmpleado

                                    )
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}

?>