<?php
class DetalleFraccion
{
	private $pdo;
	public $fracciones;
	public $idEmpleado;
	public $saldoDias;
	public $cantDiasPorFraccion;
	public $fechaSalida;
	public $fechaEntrada;
	public $tipofraccion;
	public $descripcion;
	

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


		public function Listar($idEmpleado,$idFraccion)
	{
		try
		{
			$result = array();
			//$stm = $this->pdo->prepare("SELECT *from  detallefracciones where idEmpleado=? and fracciones=?");
			$stm = $this->pdo->prepare("SELECT *from  detallefracciones where idEmpleado=?  ");
			$stm->execute(array($idEmpleado));

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function obtenerSaldo($idEmpleado,$idFraccion){

			try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT  saldoDias  from fracciones where idEmpleado = ? and idFracciones=? order by saldoDias asc limit 1");
			$stm->execute(array($idEmpleado,$idFraccion));
			return $stm->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function agregarFraccion(DetalleFraccion $data)
	{
		try 
		{
			$sql = "INSERT INTO detallefracciones (fracciones,idEmpleado,saldoDias,cantDiasPorFraccion,fechaSalida,fechaEntrada,tipofraccion,descripcion) 
			VALUES (?,?,?,?,?,?,?,?)";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->fracciones,
					$data->idEmpleado,
					$data->saldoDias,
					$data->cantDiasPorFraccion,
					$data->fechaSalida,
					$data->fechaEntrada,
					$data->tipofraccion,
					$data->descripcion
					
				)
			);
			$tri="UPDATE fracciones set saldoDias=? where idFracciones =?";
			$this->pdo->prepare($tri)
			->execute(
				array(
					$data->saldoDias,
					$data->fracciones
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}

?>