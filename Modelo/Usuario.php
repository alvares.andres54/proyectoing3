<?php
class Usuario
{
	private $pdo;

	public $idUsuario;
	public $user;
	public $correo;
	public $perfil;
	public $contrasena;
	public $idEmpleado;


	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Verificar($user,$pass)
	{
		try
		{

			$result = array($user,$pass);

			$stm = $this->pdo->prepare("SELECT * FROM usuario where user=? COLLATE utf8_bin and contrasena=? COLLATE utf8_bin");
			$stm->execute($result);

			return $stm->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			echo "";
			die($e->getMessage());
		}
	}
	public function obtenerUsuario()
	{

	}
	public function Registrar(Usuario $data)
	{
		try 
		{
			$sql = "INSERT INTO usuario (user,contrasena,perfil,correo) 
			VALUES (?, ?, ?, ?)
			";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->user,
					$data->contrasena,
					$data->perfil, 
					$data->correo, 


				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
	public  function actualizarUsuarioEmpleado($idUsuario){
		try 
		{
			$sql = "UPDATE empleado set usuario=? where idEmpleado=?";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$idUsuario->idUsuario,
					$idUsuario->idEmpleado
				)
			);
			return true;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function listaUltimo(){ 

			try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM usuario GROUP BY idUsuario DESC LIMIT 1 ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function Eliminar($idUsuario)
	{
		try 
		{
			$stm = $this->pdo
			->prepare("DELETE FROM usuario WHERE idUsuario = ?");			          

			$stm->execute(array($idUsuario));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
	public function Actualizar($data)
	{

		try 
		{
			echo "id:".$data->idUsuario;
			echo "<br>user:".$data->user;

			$sql = "UPDATE usuario SET 
			user          = ?, 
			contrasena    = ?,
			correo        = ?
			WHERE idUsuario= ?";

			$this->pdo->prepare($sql)
			->execute(
				array(
					$data->user, 
					$data->contrasena,
					$data->correo,
					$data->idUsuario
				)
			);
			return true;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function ActualizaContra($data)
	{

		try 
		{

			$sql = "UPDATE usuario SET 
			
			contrasena = ? 

			WHERE user= ?";

			$this->pdo->prepare($sql)
			->execute(
				array(
					
					$data->contrasena,
					$data->user
				)
			);
			return true;
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}/*mrtodo modificar*/

	public function VerificarUsuario($id)
	{
		try
		{

			$result = array($id);

			$stm = $this->pdo->prepare("SELECT * FROM usuario where user=?");
			$stm->execute($result);

			return $stm->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			echo "";
			die($e->getMessage());
		}
	}
	
	public function buscarEmpleado($id)/*funcion para agregrar nuevos empledos por usuario: cedula y contraseña: nombre del empleado*/
	{
		try 
		{
			$stm = $this->pdo
			->prepare("SELECT *FROM empleado WHERE idEmpleado= ?");

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
//Fin de la conversación
//Escribe un mensaje, agrega @nombre...

}