<?php


class UnidadTrabajo
{
	private $pdo;
	
	public $idUnidaOrg;
    public $nombre;
    public $encargado;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}




	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM unidaorg");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM unidaorg WHERE idUnidaOrg = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
public function ObteneIdUnidad($nombre)
	{
		try 
		{
			$stm = $this->pdo->prepare("SELECT idUnidaOrg FROM unidaorg WHERE nombre = ?");
			$stm->execute(array($nombre));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Registrar(UnidadTrabajo $data)
	{
		try 
		{
			$sql = "INSERT INTO unidaorg (nombre,encargado) 
			VALUES (?,?)";

			$this->pdo->prepare($sql)
			->execute(
				array( 
					$data->nombre, 
					0
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
			echo "error de unidad";
		}
	}/*metodo para agregar empleado*/

}

?>