<?php 

class Fraccion
{
	private $pdo;

	public $idFraccion;
	public $idEmpleado;
	public $descripcion;
	public $periodo;
	public $diasTotalesPorPeriodo;
	public $saldo;


	public function __Construct()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}



	public function ListarPeriodoEmpleado($id)
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT nombre,apellido,anosLaborado,fechaIngreso, fracciones.*,estado from empleado inner join fracciones on empleado.idEmpleado= fracciones.idEmpleado where estado = 1 and empleado.idEmpleado=?");
			$stm->execute(array($id));

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT nombre,apellido,anosLaborado,fechaIngreso, temp.*,estado 
  			from empleado inner join fracciones as temp on empleado.idEmpleado=temp.idEmpleado where  periodo=(SELECT Max(periodo) from fracciones where idEmpleado=temp.idEmpleado ) and estado=1");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function ListarPeriodosEmpleado($id)
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * from  fracciones where idEmpleado= ?");
			$stm->execute(array($id));

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function ListarPeriodos()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT DISTINCT idFracciones,periodo from  fracciones group by periodo");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}
	public function ObtenerPeriodo($id)
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * from  fracciones where idFracciones = ?");
			$stm->execute(array($id));

			return $stm->fetch(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	} 

	public function agregarFraccion(Fraccion $data)
	{
		try 
		{
			$sql = "INSERT INTO fracciones (idEmpleado,descripcion,periodo,diasTotalesPorPeriodo,saldoDias) 
			VALUES (?,?,?,?,?)";

			$this->pdo->prepare($sql)
			->execute(
				array(
					
					$data->idEmpleado,
					$data->descripcion,
					$data->periodo,
					$data->diasTotalesPorPeriodo,
					$data->saldo
					
				)
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

}



?>