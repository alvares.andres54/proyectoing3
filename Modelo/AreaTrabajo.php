<?php


class AreaTrabajo
{
	private $pdo;
	
	public $idAreaDeTrabajo;
    public $nombre;
    

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::Conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}




	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT * FROM areadetrabajo");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}


	public function Obtener($id)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM areadetrabajo WHERE idAreaDeTrabajo = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
public function ObtenerNombre($id)
	{
		try 
		{
			$stm = $this->pdo ->prepare("SELECT idAreaDeTrabajo FROM areadetrabajo WHERE nombre = ?");
			        
			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
	public function Registrar(AreaTrabajo $data)
	{
		try 
		{
			$sql = "INSERT INTO areadetrabajo  (nombre)
			VALUES (?)";

			$this->pdo->prepare($sql)
			->execute(
				array( 
					$data->nombre
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
			echo "error de area";
		}
	}/*metodo para agregar empleado*/

}

?>