-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: proyectoministerio
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `areadetrabajo`
--

LOCK TABLES `areadetrabajo` WRITE;
/*!40000 ALTER TABLE `areadetrabajo` DISABLE KEYS */;
INSERT INTO `areadetrabajo` VALUES (1,'Liberia'),(17,'La Cruz'),(19,'Santa Cruz'),(20,'Nandayure'),(21,'Nicoya'),(22,'Carrillo'),(23,'Hojancha'),(24,'Tilaran'),(25,'Cañas'),(26,'Bagaces'),(27,'Abangares'),(29,'ioi');
/*!40000 ALTER TABLE `areadetrabajo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `detallefracciones`
--

LOCK TABLES `detallefracciones` WRITE;
/*!40000 ALTER TABLE `detallefracciones` DISABLE KEYS */;
INSERT INTO `detallefracciones` VALUES (18,10,123,13,2,'2018-05-22','2018-05-24','OBLIGATORIA','autorized'),(19,7,9,12,3,'2018-05-22','2018-05-25','OBLIGATORIA','auto'),(20,14,8,12,3,'2018-05-22','2018-05-25','OBLIGATORIA','Autorizado');
/*!40000 ALTER TABLE `detallefracciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (8,'l','i',98,1,0,1,NULL,'0','2018-05-09','8','2018-05-19',8,0,'Estamos probando'),(9,'ruth','io',8,0,15,20,NULL,'1','1994-05-12','lll','2016-05-20',898888,0,NULL),(10,'ana','guevara',10,4,1,17,NULL,'1','1998-05-08','ll','2014-05-09',98909,0,NULL),(54,'samsumg','xre',945,6,0,1,NULL,'1','1993-05-04','hu','2012-05-10',4446,0,NULL),(112,'zukaritas','begn',19,12,1,17,NULL,'1','1976-05-13','uSa','2006-05-16',30,0,NULL),(123,'scarlet','joason',18,4,0,1,NULL,'1','1987-05-06','uSa','2014-05-07',30,0,NULL),(669,'tfyftr','tyy',0,1,0,1,NULL,'1','1973-05-10','yy','2017-05-18',667,0,NULL),(909,'reb','re',0,4,0,1,NULL,'1','1994-05-05','ee','2014-05-01',3435,0,NULL),(7687,'sergi','bring',878,6,0,1,NULL,'1','1988-05-12','usa','2012-05-16',78977878,0,NULL),(8997,'xperia','sony',6,3,0,1,NULL,'1','1994-05-05','lib','2015-05-05',677,0,NULL),(76878,'larry','page',90,48,0,1,NULL,'1','1982-05-06','japin','0000-00-00',78977878,0,NULL),(787766,'iooij','45646',8665,1,0,1,NULL,'1','1977-05-05','g','2017-05-10',889,0,NULL),(1150403,'jason','crup',7,0,1,1,NULL,'1','1998-05-06','lib','2018-05-12',8998989,0,NULL),(6666787,'dani','rrrr',987,48,0,1,NULL,'1','0000-00-00','lin','0000-00-00',65,0,NULL),(7687876,'mikubook','mi',90,48,21,29,NULL,'1','0000-00-00','japin','0000-00-00',78977878,0,NULL),(66666666,'dani','rrrr',987,2,0,1,NULL,'1','1996-05-21','lin','2016-05-12',65,0,NULL),(89898989,'rios','luis luis ',76657565,1,0,1,NULL,'1','1995-06-07','lib','2017-06-07',7777777,0,NULL),(112260879,'Pedro','Ramos',1,4,1,1,NULL,'0','2018-04-24','lberia','2018-04-24',NULL,0,'Mal empleado'),(504070128,'rafael','chevez',6,3,1,17,NULL,'1','1995-10-17','liberia','2015-05-13',8887887,0,NULL),(504100505,'Maria','Gazo Guzman',5,48,15,20,NULL,'1','1991-05-22','Liberia centro','0000-00-00',3330002,0,NULL),(504100603,'julia','robert',7,6,1,20,NULL,'1','2018-05-03','han','2012-05-17',333,0,NULL),(504100607,'Jocksan','Alvarez Cano',1,0,0,17,NULL,'1','1996-05-24','La cruz','2017-10-12',50146991,0,NULL),(504110422,'andrey','vbusro',4,48,18,27,NULL,'1','0000-00-00','dksjkd','0000-00-00',43894839,0,NULL),(508100981,'Marlen','suarez ',219,1,1,1,NULL,'0','2018-05-06','llll','2018-05-12',NULL,0,'Cese laboral');
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fracciones`
--

LOCK TABLES `fracciones` WRITE;
/*!40000 ALTER TABLE `fracciones` DISABLE KEYS */;
INSERT INTO `fracciones` VALUES (2,504100505,'Periodo Generado ','2018-2019',26,26),(3,504070128,'Periodo Generado ','2018-2019',15,15),(4,504100607,'Periodo Generado','2018-2019',15,15),(5,504110422,'Periodo Generado ','2018-2019',26,26),(6,1150403,'Periodo Generado ','2018-2019',NULL,NULL),(7,9,'Periodo Generado ','2018-2019',15,15),(8,10,'Periodo Generado ','2018-2019',15,15),(9,112,'Periodo Generado ','2018-2019',26,26),(10,123,'Periodo Generado ','2018-2019',15,15),(11,7687876,'Periodo Generado ','2018-2019',26,26),(12,76878,'Periodo Generado ','2018-2019',26,26),(13,7687,'Periodo Generado ','2018-2019',20,20),(14,8,'Periodo Generado ','2018-2019',15,12),(15,8997,'Periodo Generado ','2018-2019',15,15),(16,54,'Periodo Generado ','2018-2019',20,20),(17,909,'Periodo Generado ','2018-2019',15,15),(18,66666666,'Periodo Generado ','2018-2019',15,15),(19,6666787,'Periodo Generado ','2018-2019',26,26),(20,787766,'Periodo Generado ','2018-2019',15,15),(21,669,'Periodo Generado ','2018-2019',15,15),(22,89898989,'Periodo Generado ','2018-2019',15,15);
/*!40000 ALTER TABLE `fracciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unidaorg`
--

LOCK TABLES `unidaorg` WRITE;
/*!40000 ALTER TABLE `unidaorg` DISABLE KEYS */;
INSERT INTO `unidaorg` VALUES (0,'Unidad rectora de la salud',0),(1,'Unidad desarrollo tecnico institucional ',0),(15,'Unidad de atención al cliente',0),(18,'Unidad apoyo logistico administrativo',0),(19,'Dirección Regional',0),(21,'cuaklquiervaea',0);
/*!40000 ALTER TABLE `unidaorg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin','admin','ADMINISTRADOR','alvares.andres54@gmail.com'),(3,'ministerio','min','INVITADO','rafaelchevez4@gmail.com');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-02 14:27:54
